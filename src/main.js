import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'jquery/dist/jquery.min'
import 'popper.js/dist/popper.min'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.min'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { BNavbar } from 'bootstrap-vue'

import NavBarVue from "./components/NavBar.vue"

const app = createApp(App)

app.use(router)
app.mount('#app')

app.component('NavBar', NavBarVue)
app.component('b-navbar', BNavbar)


