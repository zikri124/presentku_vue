import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('@/views/Home.vue')
    },
    {
        path: '/thank',
        name: 'presentResponse',
        component: () => import('@/views/PresentResponse.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/Login.vue')
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('@/views/Dashboard.vue')
    },
    {
        path: '/admin/adduser',
        name: 'registerUser',
        component: () => import('@/views/Register.vue')
    },
    {
        path: '/admin/users',
        name: 'listUsers',
        component: () => import('@/views/ViewUsers.vue')
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router